package main

// (c) Jeremy Stevens, 04.07.22
//  fileinfo.go - get some basic fileinfo
import (
    "flag"
    "fmt"
    "time"
    "os"
    "log"
)

var (
 fileInfo os.FileInfo
 err    error
)

func main() {
    if len(os.Args) > 1 {
        var name string
        flag.StringVar( & name, "filename", "", "Usage")
        flag.Parse()
        fmt.Println("Loaded: " + name)
        time.Sleep(2 * time.Second)
        fileInfo, err = os.Stat(name)
        if err != nil {
            log.Fatal(err)
        }
        fmt.Println("File Name: ", fileInfo.Name())
        fmt.Println("Size in Bytes: ", fileInfo.Size())
        fmt.Println("Permissions: ", fileInfo.Mode())
        fmt.Println("Last Modified: ", fileInfo.ModTime())
        fmt.Println("is Directory: ", fileInfo.IsDir())
        fmt.Println("System interface type %t\n", fileInfo.Sys())
        fmt.Println("System info: %+v\n\n", fileInfo.Sys())
    } else {
        fmt.Println("USAGE: -filename filename")
    }

}
