package main 
// (c) Jeremy Stevens, 04.07.22
// Delete.go - Delete files 
import (
"flag"
"fmt"
"os"
"log"
)
func main() { 
if len(os.Args) > 1 { 
var name string 
flag.StringVar(&name, "file", "", "USAGE")
flag.Parse()
var answer string
fmt.Print("are you sure? " + name + "(y/n)\n")
fmt.Scan(&answer)
if answer == "y" {
	err := os.Remove(name)
	if err != nil {
	log.Fatal(err)
	}
} else { 
	// Cancel do not delete file 
	fmt.Println("Exiting Program")
	os.Exit(0)
}  

} else {
	fmt.Println("USAGE: -file filename")
}
}//end of main 
