package main

// (c) Jeremy Stevens, 04.07.22
//  empty.go - create an empty file. 

import (
    "flag"
    "fmt"
    "time"
    "os"
    "log"
)
var (
	newFile *os.File
	err error
)
func main() {
    if len(os.Args) > 1 {
        var name string
        flag.StringVar(&name, "file", "", "Usage")
        flag.Parse()
        fmt.Println("Created File: " + name)
        time.Sleep(1 * time.Second)
        newFile, err = os.Create(name)
        if err != nil {
		log.Fatal(err)
		fmt.Println("USAGE: -file filename")
	}
	newFile.Close()
	} else {
	fmt.Println("USAGE: -file filename")
	}
   }
