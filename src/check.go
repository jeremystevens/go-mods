package main

// (c) Jeremy Stevens, 04.07.22
//  check.go - check if file is readable and writeable
import (
    "flag"
    "fmt"
    "time"
    "os"
    "log"
)
func main() {
    if len(os.Args) > 1 {
        var name string
        flag.StringVar( & name, "file", "", "USAGE: -file filename")
        flag.Parse()
        fmt.Println("Checking File: " + name)
        time.Sleep(1 * time.Second)
            // check write permissions 
        file, err := os.OpenFile(name, os.O_WRONLY, 0666)
        if err != nil {
            if os.IsPermission(err) {
                log.Println("Error: Write permission denied.")
            }
        }
        fmt.Println(name + " File is writeable")
        file.Close()
            // Test read permissions
        file, err = os.OpenFile(name, os.O_RDONLY, 0666)
        if err != nil {
            if os.IsPermission(err) {
                log.Println("Error: Read permission denied.")
            }
        }
        fmt.Println(name + "File is readable")
        file.Close()
    } else {
        fmt.Println("USAGE: -file filename")
        os.Exit(0)
    }
}
