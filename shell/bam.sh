#!/bin/sh

#
#  bam.sh(build & Move) 
#
# (c) Jeremy Stevens 04.07.22
#
#


# make sure args are passed 
if [ -z "$1" ]
  then
    echo "No argument supplied"
    exit 0
fi
echo "Building GO File"
# build golang file
go build $1
sleep 1
echo "..Finished"
# extract just the name ignore ext
echo "Getting Filename"
FILE=$1
NAME=`echo "$FILE" | cut -d'.' -f1`
# change file permission
echo "..Finished"
sleep 1
echo "Changing file permissions" 
chmod +x $NAME
echo "..Finished"
sleep 1
# copy and move
echo "Copying and moving file /usr/local/bin/"
sudo cp $NAME /usr/local/bin
sudo mv $NAME  /usr/local/bin/$NAME
echo "..Finished"
sleep 1
echo "File moved to /usr/bin/$NAME"
echo "Done.."
exit 1
